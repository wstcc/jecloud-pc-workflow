export default class NodeBaseConfig {
  constructor(options, nodeType) {
    /**
     * 类型
     * 任务节点和判断节点特有的属性
     */
    if (['task', 'decision'].indexOf(nodeType) != -1) {
      this.categorydefinition = this.getCategorydefinition(nodeType);
    }

    /**
     * 节点类型
     */
    this.resourceId == options.resourceId || '';

    /**
     * 节点名称
     */
    this.name = options.name || '';

    /**
     * 绑定表单名称
     */
    this.formSchemeName = options.formSchemeName || '';

    /**
     * 绑定表单id
     */
    this.formSchemeId = options.formSchemeId || '';

    /**
     * 列表同步
     */
    this.listSynchronization = options.listSynchronization || '0';

    /**
     * 不可取回
     */
    this.retrieve = options.retrieve || '0';

    /**
     * 可催办
     */
    this.urge = options.urge || '0';

    /**
     * 可作废
     */
    this.invalid = options.invalid || '0';

    /**
     * 可转办
     * 会签节点没有
     */
    if (['countersign'].indexOf(nodeType) == -1) {
      this.transfer = options.transfer || '0';
    }

    /**
     * 可委托
     * 会签节点没有
     */
    if (['countersign'].indexOf(nodeType) == -1) {
      this.delegate = options.delegate || '0';
    }

    /**
     * 表单可编辑
     */
    this.formEditable = options.formEditable || '0';

    /**
     * 消息不提醒
     */
    this.remind = options.remind || '0';

    /**
     * 不简易审批
     */
    this.simpleApproval = options.simpleApproval || '0';

    /**
     * 自动全选
     * 会签节点没有
     */
    if (['countersign'].indexOf(nodeType) == -1) {
      this.selectAll = options.selectAll || '0';
    }

    /**
     * 异步树
     * 会签节点没有
     */
    if (['countersign'].indexOf(nodeType) == -1) {
      this.asynTree = options.asynTree || '0';
    }

    /**
     * 可跳跃
     * 会签节点,候选,多人节点没有
     */
    if (['countersign', 'joint', 'batchtask'].indexOf(nodeType) == -1) {
      this.isJump = options.isJump || '0';
    }

    /**
     * 启用逻辑
     * 会签节点,判断,多人节点没有
     */
    if (['countersign', 'decision', 'batchtask'].indexOf(nodeType) == -1) {
      this.logicalJudgment = options.logicalJudgment || '0';
    }

    /**
     * 顺序审批
     * 多人审批节点特有
     */
    if (nodeType == 'batchtask') {
      this.sequential = options.sequential || '0';
    }
    /**
     * 是否加签
     * 任务节点、固定人、候选节点有
     */
    if (['task', 'to_assignee', 'joint'].includes(nodeType)) {
      this.countersign = options.countersign || '0';
    }
    /**
     * 是否回签
     * 任务节点、固定人、候选节点有
     */
    if (['task', 'to_assignee', 'joint'].includes(nodeType)) {
      this.signBack = options.signBack || '0';
    }
    /**
     * 是否强制回签
     * 任务节点、固定人、候选节点有
     */
    /* if (['task', 'to_assignee', 'joint'].includes(nodeType)) {
      this.forcedSignatureBack = options.forcedSignatureBack || '0';
    } */
    /**
     * 是否暂存
     */
    this.staging = options.staging || '0';
    /**
     * 运行时调整
     * 多人节点有
     */
    if (['batchtask'].includes(nodeType)) {
      this.basicRuntimeTuning = options.basicRuntimeTuning || '0';
    }
    /**
     * 发起人可撤销
     */
    this.initiatorCanCancel = options.initiatorCanCancel || '0';
    /**
     * 发起人可催办
     */
    this.initiatorCanUrged = options.initiatorCanUrged || '0';
    /**
     * 发起人可作废
     */
    this.initiatorInvalid = options.initiatorInvalid || '0';
    /**
     * 是否开启密级
     */
    this.securityEnable = options.securityEnable || '0';
    /**
     * 密级类型
     */
    this.securityCode = options.securityCode || '';
  }

  getCategorydefinition(nodeType) {
    switch (nodeType) {
      //任务节点
      case 'task':
        return 'kaiteUserTask';
      //判断节点
      case 'decision':
        return 'kaiteDecideUserTask';
    }
  }
}
