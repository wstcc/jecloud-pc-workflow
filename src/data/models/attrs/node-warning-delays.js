import { isNotEmpty } from '@jecloud/utils';
export default class NodeWarningDelays {
  constructor(options) {
    /**
     * 启动预警延期
     */
    this.enable = isNotEmpty(options.enable) ? options.enable : '0';

    /**
     * 处理时限
     */
    this.processingTimeLimitDuration = isNotEmpty(options.processingTimeLimitDuration)
      ? options.processingTimeLimitDuration
      : '';

    /**
     * 处理时限单位name
     */
    this.processingTimeLimitUnitName = isNotEmpty(options.processingTimeLimitUnitName)
      ? options.processingTimeLimitUnitName
      : '';

    /**
     * 处理时限单位code
     */
    this.processingTimeLimitUnitCode = isNotEmpty(options.processingTimeLimitUnitCode)
      ? options.processingTimeLimitUnitCode
      : '';
    /**
     * 预警时限
     */
    this.warningTimeLimitDuration = isNotEmpty(options.warningTimeLimitDuration)
      ? options.warningTimeLimitDuration
      : '';

    /**
     * 预警时限单位name
     */
    this.warningTimeLimitUnitName = isNotEmpty(options.warningTimeLimitUnitName)
      ? options.warningTimeLimitUnitName
      : '';

    /**
     * 预警时限单位code
     */
    this.warningTimeLimitUnitCode = isNotEmpty(options.warningTimeLimitUnitCode)
      ? options.warningTimeLimitUnitCode
      : '';
    /**
     * 提醒频率
     */
    this.reminderFrequencyDuration = isNotEmpty(options.reminderFrequencyDuration)
      ? options.reminderFrequencyDuration
      : '';

    /**
     * 提醒频率单位name
     */
    this.reminderFrequencyUnitName = isNotEmpty(options.reminderFrequencyUnitName)
      ? options.reminderFrequencyUnitName
      : '';

    /**
     * 提醒频率单位code
     */
    this.reminderFrequencyUnitCode = isNotEmpty(options.reminderFrequencyUnitCode)
      ? options.reminderFrequencyUnitCode
      : '';

    /**
     * 配置列表数据
     * [
          {
            "name": "",
            "code": "",
            "duration 时长": "",
            "unitName 单位name": "",
            "unitCode 单位code": "",
            "serviceAndMethod service和方法，号分开": ""
          }
        ]
     */
    this.resource = isNotEmpty(options.resource) ? options.resource : [];
  }
}
