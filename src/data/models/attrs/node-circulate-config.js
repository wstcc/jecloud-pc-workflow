import { isNotEmpty } from '@jecloud/utils';
export default class NodeCirculateConfig {
  constructor(options) {
    /**
     * 可传阅
     */
    this.enable = isNotEmpty(options.enable) ? options.enable : '0';

    /**
     * 自动传阅
     */
    this.auto = isNotEmpty(options.auto) ? options.auto : '0';

    /**
     * 配置列表数据
     */
    this.circulationRules = isNotEmpty(options.circulationRules) ? options.circulationRules : [];
  }
}
