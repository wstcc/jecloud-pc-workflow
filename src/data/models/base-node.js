import { isNotEmpty } from '@jecloud/utils';
export default class BaseNode {
  get isModel() {
    return true;
  }

  child = [];
  constructor(options) {
    /**
     * 位置信息
     */
    this.bounds = options.bounds || {};

    /**
     * 节点Id
     */
    this.resourceId = options.resourceId;

    /**
     * 目前没用
     */
    this.childShapes = [];

    /**
     * 节点类型
     */
    this.stencil = {
      id: isNotEmpty(options.stencil) ? options.stencil.id : '',
    };

    /**
     * 出线Id
     */
    this.target = [];
  }
}
