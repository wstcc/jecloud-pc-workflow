/**
 * API_URL命名规则：API_模块_方法
 */

// 流程监控展板数据
export const APT_WORKFLOW_MONITORING_GET_STATISTICS = '/je/workflow/monitor/getStatistics';

// 通用查询单条数据
export const API_WORKFLOW_COMMON_GET_INFOBYID = '/je/common/getInfoById';

// 获得流程图
export const API_WORKFLOW_PREVIEW_IMAGE = '/je/workflow/processInfo/preview';

// 获得前后跳转操作的节点
export const API_WORKFLOW_MONITOR_GET_ALLNODE = '/je/workflow/monitor/getAllNode';

// 前后跳转操作
export const API_WORKFLOW_MONITOT_ADJUST_RUNNING_NODE = '/je/workflow/monitor/adjustRunningNode';

// 获得运行中的节点
export const API_WORKFLOW_MONITOR_GET_RUN_TASKS = '/je/workflow/monitor/getRunNodes';

// 调拨操作
export const API_WORKFLOW_MONITOR_ADJUST_RUNNING_NODE_ASSIGNEE =
  '/je/workflow/monitor/adjustRunningNodeAssignee';

// 获得节点人员
export const API_WORKFLOW_MONITOR_GET_NODE_ASSIGNEE =
  '/je/workflow/processInfo/getSubmitOutGoingNodeAssignee';
