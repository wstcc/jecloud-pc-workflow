import { h, ref } from 'vue';
import WorkflowLayout from '../views/layout/index.vue';
import WorkflowOperation from '../views/operation/layout.vue';
import { Modal } from '@jecloud/ui';
import { getWorkFlowImage } from '@/api/monitoring';
/**
 * 打开流程规划
 * @param {*} param0
 * @param {*} fn
 */
export function showWorkFlowWin({ workflowId, funcData }, fn) {
  Modal.window({
    title: '工作流引擎',
    width: '100%',
    height: '100%',
    bodyStyle: 'padding: 0 10px 10px 10px;',
    headerStyle: 'height:50px;',
    content() {
      return h(WorkflowLayout, { workflowId, funcData });
    },
    //关闭方法
    onClose(model) {
      fn && fn();
    },
  });
}
/**
 * 打开流程操作
 * @param {*} options
 */
export function showWorkFlowOperationWin(options) {
  Modal.window({
    title: options?.dataInfo?.funcName || '流程详情',
    bodyStyle: 'padding: 0 10px 10px 10px;',
    headerStyle: 'height:50px;border-bottom:1px solid #F0F2F5;',
    cancelButton: '关闭',
    content() {
      return h(WorkflowOperation, options);
    },
  });
}
/**
 * 打开流程图
 * @param {*} param0
 */
export function showWorkFlowImage({ params }) {
  const loading = ref(true);
  const imgUrl = ref('');
  getWorkFlowImage(params).then((data) => {
    imgUrl.value = data || '';
    loading.value = false;
  });
  const pluginSolt = () => {
    return loading.value ? (
      <div v-loading={loading.value}></div>
    ) : (
      h('div', {
        innerHTML: imgUrl.value,
        class: 'workflow-operation-image',
      })
    );
  };
  Modal.window({
    title: '流程图',
    bodyStyle: 'padding: 0 10px 10px 10px;',
    headerStyle: 'height:50px;border-bottom:1px solid #F0F2F5;',
    cancelButton: '关闭',
    content: () => pluginSolt(),
  });
}
